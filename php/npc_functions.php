<?php
//    Author: Lander Verstappen 
//    This file is part of Fpnp-PHP.
//
//    Fpnp-PHP is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Fpnp-PHP is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Fpnp-PHP.  If not, see <http://www.gnu.org/licenses/>.
function npc_actions($action){
    switch ($action){
       default:
            character_say( "no action defined here for".$action); 
            break; 
    }
}
function add_npc($npc,$npc_class){
    if ($npc_class=="Beasts"){
        $npc=build_npcsheet(init_npc($npc));
        return $npc;
    }
    if ($npc=="Human"){
        $npc=init_character();
        $npc['npc']=TRUE;
        $_SESSION[$npc['uid']]=$npc;
        return build_character_creation_sheet($npc);
    }
    $loaded=character_load($npc."-default");
    $npc='<div class="CharacterSheet" id="'.$loaded['uid'].'">'
            .build_charactersheet($loaded);
    return $npc;
}

function init_npc($npc_name){
    $npc=Array();
    $npc['uid']=uniqid();
    $npc['name']=$npc_name;
    $npc['stats']['name']=$npc_name;
    foreach(array_keys($_SESSION['beasts'][$npc_name]) as $stat){
        $npc['stats'][strtolower($stat)]=$_SESSION['beasts'][$npc_name][$stat];
    }
    //create array current/max hp from hp
    $npc['stats']['hp']=explode("%",$npc['stats']['hp']);
    $npc['stats']['hp'][1]=$npc['stats']['hp'][0];
    //create dr/dt array from normal/laser/fire/plasma/explosion
    $defencetypes=['normal','laser','fire','plasma','explosion'];
    for ($i=0;$i<count($defencetypes);$i++){
        $dtdr=explode(",",$npc['stats'][$defencetypes[$i]]);
        $npc['stats']['dt'][$defencetypes[$i]]=$dtdr[0];
        $npc['stats']['dr'][$defencetypes[$i]]=$dtdr[1];
        unset ($npc['stats'][$defencetypes[$i]]);
    }
    //create array with grouped  attacks
    $npc['stats']['attacks']=explode("%",$npc['stats']['attacks']);
    $attacks=count($npc['stats']['attacks']);
    for($i=0;$i<$attacks;$i++){
        $npc['stats']['attacks'][$i]=explode(",",$npc['stats']['attacks'][$i]);
    }   
    $npc['npc']=TRUE;
    $npc['beast']=TRUE;
    $_SESSION[$npc['uid']]=$npc;
    return $npc;
}

function generate_npc_info_table($npc){
    function attack_button($npc,$i){
    $attackbutton='<td><input type="button" value="'.$npc['stats']['attacks'][$i][0]
                  .'" onclick="select_target(\''.$npc['uid']
                  .'\');open_overlay(\''.$npc['uid'].'\',this.value,\'attack-select\')"></td>';
    return $attackbutton;
    }

    $info_table='<table class="personal_info">
                <tr><td>HP:'.$npc['stats']['hp'][0].'/'.$npc['stats']['hp'][1].'</td><td>SQ:'.$npc['stats']['sq'].'</td><td>AP:'.$npc['stats']['ap'].'</td><td>Type: DT/DR</td></tr>
                <tr><td>Crit:'.$npc['stats']['cc'].'%<td>AC:'.$npc['stats']['ac'].'</td><td>XP:'.$npc['stats']['xp'].'</td><td>normal:'.$npc['stats']['dt']['normal'].'/'.$npc['stats']['dr']['normal'].'</td></tr>
                <tr><td>PR:'.$npc['stats']['pr'].'</td><td>RR:'.$npc['stats']['rr'].'</td><td>GR:'.$npc['stats']['gr'].'</td><td>Fire:'.$npc['stats']['dt']['fire'].'/'.$npc['stats']['dr']['fire'].'</td><tr>
                <tr><td rowspan="2">Attacks:</td>';
                for($i=0;$i<2;$i++){
                    if (isset($npc['stats']['attacks'][$i])){
                    $info_table.=attack_button($npc,$i);
                    }else{
                    $info_table.='</td><td class="EmptyTableCell"></td>';
                    }
                }
    $info_table.='<td>Plasma:'.$npc['stats']['dt']['plasma'].'/'.$npc['stats']['dr']['plasma'].'</td></tr>
                <tr>';
                for($i=2;$i<4;$i++){
                    if (isset($npc['stats']['attacks'][$i])){
                    $info_table.=attack_button($npc,$i);
                    }else{
                    $info_table.='</td><td class="EmptyTableCell"></td>';
                    }
                }
    $info_table.='<td>Explosion:'.$npc['stats']['dt']['explosion'].'/'.$npc['stats']['dr']['explosion'].'</td></tr>';
                if(isset($npc['stats']['attacks'][4])){
                    $info_table.='<tr>';
                    for($i=4;$i<8;$i++){
                        if (isset($npc['stats']['attacks'][$i])){
                        $info_table.=attack_button($npc,$i);
                        }else{
                        $info_table.='</td><td class="EmptyTableCell"></td>';
                        }
                    }
                    $info_table.='</tr>';
                }
    $info_table.=' <tr><td colspan="4"><input type="button" value="Description" onclick="show_npc_description(\''.$npc['uid'].'\')"></td></tr>
                   <tr><td colspan="4" id="'.$npc['uid'].'description" class="npcdescription">'.$npc['stats']['description'].'</td></tr>
            </table>';
    return $info_table;
}

function generate_gameactions_table_npc($id){
    $gameactions='<table class="gameactions_table">
        <tr>
        <td><input type="button" value="Start Combat" onclick="start_turn(\''.$id.'\')"></td>
        </tr>
        <tr>
        <td><input type="button" value="Defend" onclick="defend(\''.$id.'\')"></td>
        </tr>
        <tr>
        <td><input type="button" value="Cheat" onclick="open_overlay(\''.$id.'\',selectedItem,\'GM-discretion\')">
        </tr>
        <tr>
        <td><input type="button" value="Die" onclick="select_target(\''.$id.'\',\'die\');open_overlay(\''.$id.'\',\'\',\'die\')"></td>
        </tr>
        </table>';

    return $gameactions;
}
function generate_npc_overlay_div($id,$stats){
    $overlay_div='<div id="Overlay-attack-select'.$id.'" class="Overlay">
                <table>
                    <tr><td>target:</td><td id="targetList'.$id.'"></td></tr>
                    <tr><td><input type="button" value="cancel" onclick="close_overlay(\''.$id.'\',\'attack-select\')"></td>
                    <td><input type="button" value="Lock" onclick="get_attackinfo(\''.$id.'\',selectedItem);open_overlay(\''.$id.'\',selectedItem,\'attack-roll\')"</td></tr></table>
    </div>
    <div id="Overlay-attack-roll'.$id.'" class="Overlay"></div>
    <div id="Overlay-GM-discretion'.$id.'" class="Overlay">
    <table><tr>
    <td>Stat</td><td>New Value</td>
    </tr><tr>
    <td><select id="GM-discretionStat'.$id.'">';
    foreach(array_keys($stats)as $stat){
        if(is_array($stats[$stat])){
        $overlay_div.='<optgroup label="'.$stat.'">';
        foreach(array_keys($stats[$stat])as $aStat){
            $overlay_div.='<option value="'.$stat.'?'.$aStat.'">'.$aStat.'</option>';}
        $overlay_div.='</optgroup>';
    }else{
        $overlay_div.='<option value="'.$stat.'">'.$stat.'</option>';
    }
    }
    $overlay_div.='</select></td><td><input type="text" id="adjustedStatAmount'.$id.'"></td>
    </tr><tr>
    <td><input type="button" value="Cancel" onclick="close_overlay(\''.$id.'\',\'GM-discretion\')"></td>
    <td><input type="button" value="Adjust" onclick="adjust_value(\''.$id.'\')"></td>
    </tr></table>
        <div id="Overlay-die'.$id.'" class="Overlay">
        <table><tr><td id="targetList'.$id.'die">Killer: </td><tr>
            <tr><td><input type="button" value="Die" onclick="close_overlay(\''.$id.'\',\'die\');die(\''.$id.'\')"><input type="button" value="Cancel" onclick="close_overlay(\''.$id.'\',\'die\')"></td></tr></table>;
        </div>';
    return $overlay_div;
}

function build_npcsheet($npc,$first=TRUE){
    $npcsheet='';
        if ($first===TRUE){
            $npcsheet='<div class="CharacterSheet npcPersonalInfo" id="'.$npc['uid'].'">';
        }
         $npcsheet.='<input type="hidden" name="npc_uid" value="'.$npc['uid'].'">
                    <h2>'.$npc['stats']["name"].'</h2><hr>'
                .generate_npc_info_table($npc)
                .generate_gameactions_table_npc($npc['uid'])
                .generate_npc_overlay_div($npc['uid'],$npc['stats']);
        if ($first===TRUE){
            $npcsheet.='</div>';
        }
    return $npcsheet;
}

?>
