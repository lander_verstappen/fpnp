<?php
//    Author: Lander Verstappen 
//    This file is part of Fpnp-PHP.
//
//    Fpnp-PHP is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Fpnp-PHP is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Fpnp-PHP.  If not, see <http://www.gnu.org/licenses/>.


//Missing features/bugs
//provide info tables in menubar.(Crit hit/Crit Miss/descriptions////)
//Ammo needs to implementation
//no automatic use of skill(first aid/docter/lockpick/sneak)
//No traits or Perks...
//Only race is currently human.

include 'character_functions.php';
include 'npc_functions.php';
include 'combat_functions.php';
include 'game_functions.php';

if(!isset($_SESSION)) { //Launch new session - loads config files 
    session_start(); 
    load_config();
} 

if (!empty ($_POST['actiontype'])){postoffice($_POST['actiontype']);}
?>
