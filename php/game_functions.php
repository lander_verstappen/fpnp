<?php
//***GAME FUNCTIONS***
//    Author: Lander Verstappen 
//    This file is part of Fpnp-PHP.
//
//    Fpnp-PHP is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Fpnp-PHP is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Fpnp-PHP.  If not, see <http://www.gnu.org/licenses/>.

function load_config(){
     // $_SESSION["secondary_skills"]=parse_ini_file($_SERVER['DOCUMENT_ROOT']."/fpnp/config/secondary_skills.ini", true);
    $_SESSION["skills_list"]=parse_ini_file($_SERVER['DOCUMENT_ROOT']."/fpnp/config/skills.ini", true);
    $_SESSION["weapons"]=parse_ini_file($_SERVER['DOCUMENT_ROOT']."/fpnp/config/weapons.ini", true);
    foreach(array_keys($_SESSION["weapons"]) as $weapon){
       $mode_array=explode(",",$_SESSION['weapons'][$weapon]['ap']);
        unset($_SESSION['weapons'][$weapon]['ap']);
        foreach($mode_array as $mode){
            $mode_ap=explode(":",$mode);
            $_SESSION['weapons'][$weapon]['ap'][$mode_ap[0]]=$mode_ap[1];
        }
    }
    $_SESSION["apparel"]=parse_ini_file($_SERVER['DOCUMENT_ROOT']."/fpnp/config/apparel.ini", true);
    $_SESSION["aid"]=parse_ini_file($_SERVER['DOCUMENT_ROOT']."/fpnp/config/aid.ini", true);
    $_SESSION["beasts"]=parse_ini_file($_SERVER['DOCUMENT_ROOT']."/fpnp/config/bestiary.ini", true);
    $_SESSION["savedir"]=$_SERVER['DOCUMENT_ROOT']."/fpnp/cache/";
    $_SESSION["special_full"]=["str"=>"Strenght","per"=>"Perception","end"=>"Endurance","cha"=>"Charisma","int"=>"Inteligence","agi"=>"Agility","lck"=>"Luck"];
}

function postoffice($actiontype){
    switch ($actiontype){
        case "character":
            character_actions($_POST['action']);
            break;
        case "npc":
            npc_actions($_POST['action']);
            break;
        case "game":
            game_actions($_POST['action']);
            break;
        case "combat":
            combat_actions($_POST['action']);
            break;
        default:
            character_say("no valid action here for".$action);
    }
}

function game_actions($action){
    switch ($action){
        case "new_game":
            session_destroy();
            session_start();
            load_config();
            build_gamezone();
            break;
        case "add_player":
            echo build_character_creation_sheet();
            break;
        case "add_npc":
            echo add_npc($_POST['npc'],$_POST['npc_class']);
            break; 
        case "refresh":
            build_sheet($_SESSION[$_POST['uid']]);
            break;
        case "adjust_stat":
            adjust_stat($_SESSION[$_POST['uid']]);
            build_sheet($_SESSION[$_POST['uid']]);
            break;
        case "safe":
            character_save($_SESSION[$_POST['uid']]);
            character_say("Saved",$_POST['uid']);
            build_sheet($_SESSION[$_POST['uid']]);
            break;
        case "load":
            $character=character_load($_POST['savefile']);
            echo '<div class="CharacterSheet" id="'.$character['uid'].'">';
            character_say("Fresh out the box",$character['uid']);
            build_sheet($character);
            echo '</div>';
            break;
        case "save_session":
            $savedir=$_SESSION["savedir"];
            //unload config
            unset ($_SESSION["skills_list"]);
            unset ($_SESSION["weapons"]);
            unset ($_SESSION["apparel"]);
            unset ($_SESSION["aid"]);
            unset ($_SESSION["beasts"]);
            unset ($_SESSION["savedir"]);
            unset ($_SESSION["special_full"]);
            file_put_contents($savedir."sessions/".$_POST['name'],session_encode());
            load_config();
            character_say("The world is Saved!");
            break;
        case "load_session":
            $load_file=file_get_contents($_SESSION["savedir"]."sessions/".$_POST['name']);
            session_decode($load_file);
            rebuild_gamezone();
            break;
        default:
            character_say("no valid action here for ".$action);
    }
}

function build_gamezone(){
    $gamezone='<h1><hr>Players<hr></h1><div id="players"></div>
    <h1><hr>NPC<hr></h1>
    <div id="npc_zone"></div>'
    .build_load_overlay();
    echo $gamezone;
}

function character_save($character){//write character to file
        if ($character==NULL){
            return;
        }
        file_put_contents($_SESSION["savedir"]."characters/".$character['stats']["name"]."-".$character['uid'],serialize($character));
}

function character_load($savefile){//read character from file
    $character=unserialize(file_get_contents($_SESSION["savedir"]."characters/".$savefile));
    $character['uid']=uniqid();//give new id.
    $_SESSION[$character['uid']]=$character;
    return $character;
}

function build_sheet($character){
    if($character['beast']===true){
        echo build_npcsheet($character,FALSE);
    }else{
        echo build_charactersheet($character);
    }
}

function build_load_overlay(){
    $load_overlay='';
    //character-load
    $char_files=array_slice(scandir($_SESSION['savedir']."characters/"), 2);
    $templates=[];
    foreach($char_files as $file){
    $strippedfile=explode("-",$file);
    if ($strippedfile[1]=="default"){
    $templates["default"][]=$strippedfile[0];
    }else{
    $templates["players"][]=$strippedfile;
    }
    }
    $load_overlay.='<div id="Overlay-build-npc" class="Overlay">
                  <table>
                    <tr><td colspan="2"><select id="add-npc-select-list">
                    <optgroup label="Non-Beasts">
                    <option value="Human">New Human</option>';
                    foreach ($templates["default"] as $template){
                        $load_overlay.='<option>'.$template.'</option>';
                    }
    $load_overlay.='    
                    </optgroup>
                    <optgroup label="Beasts">';
                foreach(array_keys($_SESSION['beasts']) as $beast){
                $load_overlay.='<option value="'.$beast.'">'.$beast.'</option>';
                }
     $load_overlay.=' </optgroup>
                    <optgroup label="traps">
                    <option>Not implemented</option>
                    </optgroup>
                    </select><td>
                    <tr><td><input type="button" value="cancel" onclick="close_overlay(\'\',\'build-npc\')"></td>
                    <td><input type="button" value="Add" onclick="add_npc();close_overlay(\'\',\'build-npc\')"></td></tr>
                    </table> 
                </div>';
    //Sessions-load
    $session_files=array_slice(scandir($_SESSION['savedir']."sessions/"), 2);
    $load_overlay.='<div id="Overlay-load-session" class="Overlay">
                  <table>
                    <tr><td colspan="2"><select id="load-session">';
                    foreach ($session_files as $session){
                        $load_overlay.='<option>'.$session.'</option>';
                    }
    $load_overlay.='</select><td>
                    <tr><td><input type="button" value="cancel" onclick="close_overlay(\'\',\'load-session\')"></td>
                    <td><input type="button" value="load" onclick="load_session();close_overlay(\'\',\'load-session\')"></td></tr>
                    </table> 
                </div>';
    //Session-Save
    $load_overlay.='<div id="Overlay-save-session" class="Overlay">
                  <table>
                    <tr><td colspan="2">Save Session</td></tr>
                    <tr><td>Name:</td><td><input type="text" id="session-name"></td><tr>
                    <tr><td><input type="button" value="cancel" onclick="close_overlay(\'\',\'save-session\')"></td>
                    <td><input type="button" value="Save" onclick="save_session();close_overlay(\'\',\'save-session\')"></td></tr>
                    </table> 
                </div>';
    
    
    $load_overlay.='<div id="Overlay-load-character" class="Overlay">
                <table>
                <tr><td colspan="2"><select id="add-character-select-list">';
                    foreach ($templates["players"] as $template){
                        $load_overlay.='<option value="'.$template[0].'-'.$template[1].'">'.$template[0].'</option>';
                    }
    $load_overlay.='    
                    <tr><td><input type="button" value="cancel" onclick="close_overlay(\'\',\'load-character\')"></td>
                    <td><input type="button" value="Add" onclick="load_player();close_overlay(\'\',\'load-character\')"></td>
                </table> 
                </div>';
    return $load_overlay;
}

function adjust_stat($character){ //GM-discretion / Cheat
            $stat=explode("?",$_POST['stat']);
            if (isset($stat[1])){
                if ($stat[0]=="special"){
                    $_SESSION[$_POST['uid']]['stats'][$stat[0]][$stat[1]]=$_POST['amount'];
                    $_SESSION[$_POST['uid']]=calculate_skills($_SESSION[$_POST['uid']]);
                    $_SESSION[$_POST['uid']]=calculate_secondary_stats($_SESSION[$_POST['uid']]);
                }else{
                $_SESSION[$_POST['uid']]['stats'][$stat[0]][$stat[1]]=$_POST['amount'];
                }
            }else{
                $_SESSION[$_POST['uid']]['stats'][$_POST['stat']]=$_POST['amount'];
            }
}

function character_say($error){
    $say_div='<div class="Say">"'.$error.'"</div>';
    echo $say_div;
}

function rebuild_gamezone(){
    $characterlist=[];
    foreach (array_keys($_SESSION) as $session_key){
        switch($session_key){
            case "skills_list":case "weapons":case "apparel": case "aid": case "beasts":case "savedir":case "special_full":
                break;
            default:
                $characterlist[]=$session_key;
        }
    }
    $gamezone='<h1><hr>Players<hr></h1><div id="players">';
    foreach ($characterlist as $character_key){
            $character=$_SESSION[$character_key];
            if ($character['npc']===FALSE){
                $gamezone.='<div class="CharacterSheet" id="'.$character['uid'].'">'.build_charactersheet($character).'</div>';
            }
    }
    $gamezone.='</div>
    <h1><hr>NPC<hr></h1>
    <div id="npc_zone">';
    foreach ($characterlist as $character_key){
            $character=$_SESSION[$character_key];
        if ($character['npc']===TRUE){
            if ($character['beast']===TRUE){
                $gamezone.=build_npcsheet($character);
            }else{
                $gamezone.='<div class="CharacterSheet" id="'.$character['uid'].'">'.build_charactersheet($character).'</div>';
            }
        }
    }
        $gamezone.='</div>'.build_load_overlay();
        echo $gamezone;
}
?>
