<?php
//***COMBAT FUNCTIONS***
//    Author: Lander Verstappen 
//    This file is part of Fpnp-PHP.
//
//    Fpnp-PHP is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Fpnp-PHP is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Fpnp-PHP.  If not, see <http://www.gnu.org/licenses/>.

//lots of things need to be implemented-- best to reread Handbook
//Following is def missing:
//some change to hit modulators
//Vehicle combat
//explosives
//fire/pois/rad damage
function combat_actions($action){
    switch ($action){
        case "get_attackinfo":
            echo get_attackinfo($_SESSION[$_POST['uid']],$_POST['weapon'],$_SESSION[$_POST['target']]);
            break;
        case "start_turn":
            start_turn($_SESSION[$_POST['uid']]);
            build_sheet($_SESSION[$_POST['uid']]);
            break;
        case 'defend':
            defend($_SESSION[$_POST['uid']]);
            build_sheet($_SESSION[$_POST['uid']]);
            break;
        case 'attack':
            attack($_SESSION[$_POST['uid']],$_POST['cost'],$_POST['attackRoll'],$_SESSION[$_POST['target']],$_POST['weapon']);
            build_sheet($_SESSION[$_POST['uid']]);
            break;
        case 'reload':
            //Taking item =4
            $character[1]["ap"]-=2;
            break;
        case 'chanceToHit':
            echo get_chance_to_hit($_SESSION[$_POST['uid']],$_POST['skill'],$_SESSION['weapons'][$_POST['weapon']],$_POST['range'],$_POST['mode'],$_SESSION[$_POST['target']]['stats']['ac'],$_POST['targeted']);
            break;
        case 'die':
            funeral($_SESSION[$_POST['uid']],$_SESSION[$_POST['data_target']]);
            break;
        default:
            character_say("no valid action here for".$action);

    }
}

function start_turn($character){ //Reset AP
    if ($character['beast']==TRUE){
        $character['stats']['ap']=$_SESSION['beasts'][$character['name']]['AP'];
        $character['stats']['ac']=$_SESSION['beasts'][$character['name']]['AC'];
        $_SESSION[$character['uid']]=$character;
    }else{
        character_refresh($character);
    }
}

function get_chance_to_hit($attacker,$skill,$weapon,$range,$mode,$targetac,$targeted){
    //returns a chance to hit
    //base_change
        $base_chance=$attacker['stats']["skills"][$skill]; //base chance is skill 
    //first modifier
            $range_pen=((2*$attacker['stats']["special"]["per"])-1)+$weapon['rng'];
        if ($skill=='Throwing'){ // add str
            $range_pen_str=((2*$attacker['stats']["special"]["str"])-1)+$weapon['rng'];
            if ($range_pen_str < $range_pen){
                $range_pen=$range_pen_str;
            }
        }
        $range_pen-=$range;
        if ($range_pen<0){
            $range_pen*=-3;
        }else{
            $range_pen=0;
        }
    //light mod - not impl
    //weapon status -not impl
    //targeted shots
        if ($mode=="Targeted"){
            switch ($targeted){
                case 'leftleg':
                case 'rightleg':
                case 'body':
                    $targeted_pen=10;
                    break;
                case 'leftarm':
                case 'rightarm':
                case 'righthand':
                case 'leftthand':
                    $targeted_pen=15;
                    break;
                case 'head':
                    $targeted_pen=30;
                    break;
            }
        }
        $rth=$base_chance-$range_pen-$targetac-$targeted_pen;
        if ($rth<=$attacker['stats']['cc']&&$rth>0){
            //critical!
            return "Chance to hit: ".$rth."% --Critical Hit!";
        }else{
            return "Chance to hit: ".$rth."%";
        }
}

function get_attackinfo($character,$attack,$target=0){
        $attack_info='<table id="fetched_info'.$character['uid'].'">
                      <tr><th>'.$attack.'</th></tr>';
    if ($character['beast']===TRUE){
        for ($i=0;$i<count($character['stats']['attacks']);$i++){
            if ($character['stats']['attacks'][$i][0]==$attack){
                $array_key=$i;
                break;
            }
        }
        $rth=($character['stats']['attacks'][$array_key][1]-$target['stats']['ac']);
        if ($rth<=$character['stats']['cc']){
            $rth.="% --Critical Hit";
        }else{
            $rth.="%";
        }
        $dice=$character['stats']['attacks'][$array_key][3];
        $cost=$character['stats']['attacks'][$array_key][2];
        $attack_info.='<tr><td>Roll to Hit:</td><td>'.$rth.'</td></tr>
                   <tr><td>Attack-Roll:</td><td><input type="number" id="attackRoll'.$character['uid'].'" value=0 class="'.$cost.'">'.$dice.'</td></tr>
                      <tr><td>AP:</td><td>'.$cost.'</td></tr>
                    <tr><td><input type="button" value="Miss" onclick="attack(\''.$character['uid'].'\',\'miss\')"></td>
           <td><input type="button" value="Hit" onclick="attack(\''.$character['uid'].'\',selectedItem)"></td></tr>
           <tr><td colspan="2"><input type="button" value="Cancel" onclick="close_overlay(\''.$character['uid'].'\',\'attack-roll\');clean_attack_fetched_info(\''.$character['uid'].'\')"></td></tr>';

    }else{
            
        $dice=$_SESSION['weapons'][$attack]['dice'];
        $attack_info.='<tr><td><img src="img/vaulttec.png" onload="select_target(\''.$character['uid'].'\');" width="10%" ></td></tr><tr><td id="chanceToHit'.$character['uid'].'"></td></tr>
                    <tr><td>Range: <input type="number" id="attackRange'.$character['uid'].'" value=1 onchange="character_getChanceToHit(\''.$character['uid'].'\');" ></td></tr>
                    <tr><td>Skill: <select id="attackSkill'.$character['uid'].'" onchange="character_getChanceToHit(\''.$character['uid'].'\');" >
                        <option default>'.$_SESSION['weapons'][$attack]['skill'].'</option>
                        <option>Throwing</option></select></td></tr>
                    <tr><td>Mode: <select id="attackMode'.$character['uid'].'" onchange="character_getChanceToHit(\''.$character['uid'].'\');set_attackTargeted_visible(\''.$character['uid'].'\')" >';
            foreach(array_keys($_SESSION['weapons'][$attack]['ap']) as $weapon_mode){
        $attack_info.='<option value="'.$weapon_mode.'">'.$weapon_mode.' - '.$_SESSION['weapons'][$attack]['ap'][$weapon_mode].'AP</option>';
            }
             $attack_info.='</select></td></tr><tr class="attackTargeted" id="attackTargetedRow'.$character['uid'].'"><td>Aim: <select id="attackTargeted'.$character['uid'].'" onchange="character_getChanceToHit(\''.$character['uid'].'\');" >';
            foreach(array_keys($character['equipped']) as $bodypart){
                $attack_info.='<option>'.$bodypart.'</option>';
            }
            $attack_info.='</select>
            </td></tr>
            <tr><td>Attack-Roll: <input type="number" id="attackRoll'.$character['uid'].'" value=0> - '.$dice.'</td></tr>
            <tr><td id="targetList'.$character['uid'].'">Target:</td></tr>
                    <tr><td><input type="button" value="Miss" onclick="close_overlay(\''.$character['uid'].'\',\'use-item\');attack(\''.$character['uid'].'\',\'miss\')">
            <input type="button" value="Hit" onclick="close_overlay(\''.$character['uid'].'\',\'use-item\');attack(\''.$character['uid'].'\',selectedItem)"></td></tr>
            <tr><td><input type="button" value="Cancel" onclick="close_overlay(\''.$character['uid'].'\',\'use-item\');clean_attack_fetched_info(\''.$character['uid'].'\')"></td></tr>';
    }
            $attack_info.=' </table>';
    return $attack_info;
}

function attack($character,$cost,$attackroll,$target,$weapon){
    if ($target['beast']===true){//beasts have this dived as it should
        $dt=$target['stats']['dt']['normal'];
        $dr=$target['stats']['dr']['normal'];
        $dmgbonus=0;
    }else{
        $dt=$target['stats']['dt'];
        $dr=$target['stats']['dr'];
    }
    if ($character['beast']===false){
        if ($_SESSION['weapons'][$weapon]['dmg']=="md"){
            $dmgbonus+=$character['stats']['md'];
        }else{
            $dmgbonus+=$_SESSION['weapons'][$weapon]['dmg'];
        }
    }
    $damage=get_damage($attackroll,$dmgbonus,$dt,$dr);
    echo character_say("Damage done: ".$damage);
    $target['stats']['hp'][0]-=$damage;
    $character['stats']['ap']-=$cost;
    $_SESSION[$target['uid']]=$target;
    $_SESSION[$character['uid']]=$character;

}
function get_damage($roll,$damage,$dt,$dr){
    $init_damage=(($roll+$damage)-$dt);
    return ($init_damage-(floor($init_damage*($dr/100))));
}

function defend($character){ //add ap to ac
    $character["stats"]["ac"]+=$character["stats"]["ap"];
    $character["stats"]["ap"]=0;
    $_SESSION[$character['uid']]=$character;
}

function funeral($character,$killer){
    if (!$killer['beast']){
        $killer['stats']['xp']+=$character['stats']['xp'];
        if (levelupcheck($killer)===TRUE){
            //apply levelup bonus
            $killer['stats']['level']+=1;
            $killer['stats']['hp'][1]+=3+floor($character['stats']['special']['end']/2);
            $killer['stats']['skill_points']+=5+(2*$character['stats']['special']['int']);
            $_SESSION[$killer['uid']]=$killer;
            echo build_levelup_sheet($killer);
        }else{//killer did not levelup
            $_SESSION[$killer['uid']]=$killer;
            build_sheet($killer); 
            character_say("You die now, hahaha!");
        }
    }else{//Beasts don't do XP
        build_sheet($killer);
    }
    unset ($_SESSION[$character['uid']]);//PermaDeath
}
?>
