<?php
//***CHARACTER FUNCTIONS***
//    Author: Lander Verstappen 
//    This file is part of Fpnp-PHP.
//
//    Fpnp-PHP is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Fpnp-PHP is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Fpnp-PHP.  If not, see <http://www.gnu.org/licenses/>.
function character_actions($action){
    switch ($action){
        case "update":
            echo build_charactersheet(character_create($_SESSION[$_POST['uid']]));
            break;
        case "equip":
            character_equip($_SESSION[$_POST['uid']],$_POST['item'],$_POST['bodypart']);
            character_say("Do you like my ".$_POST['item']);
            echo build_charactersheet($_SESSION[$_POST['uid']]);
               break; 
        case "drop":
            character_drop($_SESSION[$_POST['uid']],$_POST['item'],$_POST['bodypart']);
            character_say("My ".$_POST['item']."!");
            echo build_charactersheet($_SESSION[$_POST['uid']]);
            break;
        case "use":
            character_use($_SESSION[$_POST['uid']],$_POST['item'],$_POST['duration'],$_POST['addiction'],$_POST['inout'],$_POST['bodypart'],$_SESSION[$_POST['target']]);
            character_say("I just love ".$_POST['item']."ing");
            echo build_charactersheet($_SESSION[$_POST['uid']]);
            break;
        case "item_info":
            if (isset($_SESSION['aid'][$_POST['item']])){            
                echo $_SESSION['aid'][$_POST['item']]['description'];
            }elseif (isset($_SESSION['weapons'][$_POST['item']])){            
                echo $_SESSION['weapons'][$_POST['item']]['description'];
            }elseif (isset($_SESSION['apparel'][$_POST['item']])){            
                echo $_SESSION['apparel'][$_POST['item']]['description'];
            }else{
                echo "no description found";
            }
            break;
        case "store":
            character_pickup($_SESSION[$_POST['uid']],$_POST['item'],1,TRUE);
            character_drop($_SESSION[$_POST['uid']],$_POST['item'],$_POST['bodypart']);
            character_say("Get in my bag little ".$_POST['item']);
            echo build_charactersheet($_SESSION[$_POST['uid']]);
            break;
        case "pickup":
            character_pickup($_SESSION[$_POST['uid']],$_POST['item'],$_POST['amount']);
            character_say("OMG..., it's a ".$_POST['item']);
            echo build_charactersheet($_SESSION[$_POST['uid']]);
            break;
        case "item_table":
            echo generate_useItem_table($_SESSION[$_POST['uid']],$_POST['item']);
            break;
        case "use-skill":
            generate_use_skill_table($_SESSION[$_POST['uid']],$_POST['skill']);
            break;
        case "use-skill-step2":
            generate_use_skill_step2($_SESSION[$_POST['uid']],$_POST['skill'],$_SESSION[$_POST['target']]);
            break;
        case "barterDeal":
            barterDeal($_SESSION[$_POST['uid']],$_POST['PlayerList'],$_SESSION[$_POST['target']],$_POST['TargetList'],$_POST['caps']);
            echo build_charactersheet($_SESSION[$_POST['uid']]);
            break;
        case "levelup":
            process_levelup($_POST);
            build_sheet($_SESSION[$_POST['uid']]);
            character_say("Behold...");
            break;
        default:
            character_say("no valid action here for".$action);
    }
}
//initial Character
    function init_character(){//Function to initialize character
    //race: 0=human--30% elec res -- Perk every 3 levels
        $character=Array();
        $character["stats"]=["name"=>"newcommer",
                           "race"=>"Human",
                           "special"=>["str"=>5,"per"=>5,"end"=>5,"cha"=>5,"int"=>5,"agi"=>5,"lck"=>5],
                           "perks"=>[],
                           "traits"=>[0,0],
                           "skills"=>[],
                           "tagged_skills"=>[],
                           "karma"=>0,
                           "xp"=>0,
                           "level"=>1,
                           "sex"=>"Male",
                           "hp"=>[30,30], //15+(STR +(2 X EN))
                           "ac"=>5, //AGI + Armor
                           "rad_res"=>10, // 2 X EN
                           "pois_res"=>25, //5 X EN
                           "gas_res"=>[0,0],
                           "elec_res"=>30, //decided by race
                           "healing_rate"=>1, // END: 1-5=1, 6-8=2, 9-10=3, 11+=4 
                           "md"=>1, // STR: 1-6=1, 7=2, 8=3, 9=4, 10=5,...
                           "ap"=>7, // AGI: 1=5, 2-3=6, 4-5=7, 6-7=8, 8-9=9, 10+=10
                           "carry_weight"=>150, //25 + (25 X STR)
                           "dr"=>0, //Should be array: Armor Head,RightArm,RightLeg,LeftLeg,RightArm
                           "dt"=>0,//schould be arrayArmor
                           "sq"=>10, //2 X PE
                           "cc"=>5, //LCK 
                           "caps"=>0,
                           "inv_weight"=>0,
                           "skill_points"=>0,
                           "set_skill_points"=>[]
                        ];
        //calculate skills according to config file.
        $character=calculate_skills($character);
        //set uid
        $character["uid"]=uniqid();
        //create inventory
        $character["inv"]=["weapons"=>[],
                           "apparel"=>[],
                           "aid"=>[]
                         ];
        $character["equipped"]=["head"=>"none","rightarm"=>"none","leftarm"=>"none","righthand"=>"none","lefthand"=>"none","body"=>"none","rightleg"=>"none","leftleg"=>"none"];
        $character["effects"]=["in"=>[],"out"=>[]];
        $character["beast"]=FALSE;
        $character["npc"]=FALSE;
        $_SESSION[$character['uid']]=$character;
        return $character;
    }

//Calculation of stats
    function calculate_skills($character){
        foreach(array_keys($_SESSION["skills_list"])as $skill){
        //check for tagged skills
            if (isset($_POST[$skill."tag"])&&$_POST[$skill."tag"]=="true"){//Why isset and True? -write comments on the code. Don't believe the Hype, you won't know why in 3 days
                array_push($character['stats']['tagged_skills'],$_SESSION["skills_list"]["$skill"]["name"]);
            }
            if (in_array($skill,$character['stats']['tagged_skills'])){
                $character['stats']["skills"][$_SESSION["skills_list"]["$skill"]["name"]]=eval("return ".$_SESSION["skills_list"]["$skill"]["initial"]."+20;");
            }else{
                $character['stats']["skills"][$_SESSION["skills_list"]["$skill"]["name"]]=eval("return ".$_SESSION["skills_list"]["$skill"]["initial"].";");
            }
        }
        foreach (array_keys($character['stats']['set_skill_points'])as $set_skill){
            $character['stats']['skills'][$set_skill]+=$character['stats']['set_skill_points'][$set_skill]; 
        }
        return $character;
    }

    function calculate_secondary_stats($character){
        $character['stats']["hp"][1]=15+($character['stats']['special']["str"]+(2*$character['stats']['special']["end"]))+(($character['stats']['level']-1)*(3+floor($character['stats']['special']['end']/2)));//only set max HP as this is being used by chems aswell
        if ($character['stats']['hp'][0]>$character['stats']['hp'][1]){
            $character['stats']['hp'][0]=$character['stats']['hp'][1];
        }
        $character['stats']["rad_res"]=2*$character['stats']['special']["end"];
        $character['stats']["pois_res"]=5*$character['stats']['special']["end"];
        $character['stats']["carry_weight"]=25+(25*$character['stats']['special']["str"]);
        $character['stats']["seq"]=2*$character['stats']['special']["per"];
        $character['stats']["cc"]=$character['stats']['special']["lck"];
        $character['stats']["ac"]=$character['stats']["special"]["agi"];//armor will be added using process_equipment_boost
        $character['stats']["dt"]=0;
        $character['stats']["dr"]=0;
        //jumpy maybe need case switches or if statements
        //set Healing Rate
        switch($character['stats']["special"]["end"]){
            case 1:case 2:case 3:case 4:case 5:
                $character['stats']["healing_rate"]=1;
                continue;
            case 6: case 7: case 8:
                $character['stats']["healing_rate"]=2;
                continue;
            case 9: case 10: 
                $character['stats']["healing_rate"]=3;
                continue;
            default:
                $character['stats']["healing_rate"]=4;
        }
        //set Melee Damage
        switch($character['stats']["special"]["str"]){
            case 1: case 2: case 3: case 4:case 5:
                $character['stats']["md"]=1;
                continue;
            default:
                $character['stats']["md"]=$character['stats']['special']["str"]-5;
        }
        //set Action Points
        switch($character['stats']["special"]["agi"]){
            case 1:
                $character['stats']["ap"]='5';
                continue;
            case 2: case 3:
                $character['stats']["ap"]='6';
                continue;
            case 4: case 5:
                $character['stats']["ap"]='7';
                continue;
            case 6: case 7:
                $character['stats']["ap"]='8';
                continue;
            case 8: case 9:
                $character['stats']["ap"]='9';
                continue;
            default:
                $character['stats']["ap"]='10';
        }
        //$character['stats']["elec_res"]=dunno;
        return $character;
    }

    function character_create($character){//update character stats from post values
        //prim stats
        $character['stats']["name"]=$_POST["name"];
        $character['stats']["race"]=$_POST["race"];
        //special is array - loop over array and assign
        foreach(array_keys($character['stats']["special"]) as $special_name){
            $character['stats']["special"][$special_name]=$_POST[$special_name];
        }
        $character['stats']["perks"]=$_POST["perks"];
        $character['stats']["traits"]=$_POST["traits"];
        $character=calculate_secondary_stats($character);
        $character=calculate_skills($character);
        $character['stats']['hp'][0]=$character['stats']['hp'][1]; //calculate secondary only sets max HP
        $_SESSION[$character['uid']]=$character;
        return $character;
    }
    //LevelUp
    function levelupcheck($character){
        $lvl=$character['stats']['level'];
        $cur_xp=$character['stats']['xp'];
        if ($lvl<22){
            $req_exp=($lvl*($lvl+1))/0.002;
        }else{
            $req_exp=250000+(40000*($lvl-22));
        }
        if ($cur_xp<$req_exp){//No level up
            return FALSE;
        }
        return TRUE;
    }
    
    function process_levelup($data){
        $character=$_SESSION[$data['uid']];
        foreach (array_keys($character['stats']['skills']) as $skill){
            $skill_diff=($data[$skill]-$character['stats']['skills'][$skill]);
            if ($skill_diff>0){
                $character['stats']['set_skill_points'][$skill]+=$skill_diff;
            }
        }
        $character['stats']['skill_points']=$data['leftovervalue'];
        $character=calculate_skills($character);
        $character=calculate_secondary_stats($character);
        $_SESSION[$character['uid']]=$character;
    }
//HTML generation for Sheetcontent
    //character_creation
    function generate_presonal_info_table_creation($character){
        $personal_information_table='
            <table class="personal_info">
            <tr>
            <td>Name: <input type="text" name="name" value="'.$character['stats']["name"].'"></td>
            <td>Race: <select name="race" value="'.$character['stats']["race"].'">
            <option value="Human">Human</option>
            <option value="Humzn">No mutants allowed only Human</option>
            </select></td>
            <td>Sex: <select name="sex" value="'.$character['stats']["sex"].'">
            <option value="Male">Male</option>
            <option value="Female">Female</option>
            </select>
            </tr>
            </table>';
        return $personal_information_table;
    }
    
    function generate_special_table_creation($character){
        $special_table='
                <table class="special">
                    <tr>
                        <td colspan="7">Special: <span class="leftover">Left:<span class="leftovervalue">5</span></span></td>
                    </tr>
                    <tr>';
                foreach(array_keys($character['stats']["special"]) as $special_name){
                        $special_table.='
                            <td><input type="button" value="+" onclick="stat_incr(\''.$special_name.'\',\''.$character['uid'].'\')"/>'
                            .$_SESSION['special_full'][$special_name]
                            .'<input type="button" value="-" onclick="stat_decr(\''.$special_name.'\',\''.$character['uid'].'\')"/></td>';
                        }
            $special_table.='</tr>';
                        foreach(array_keys($character['stats']["special"]) as $special_name){
                          $special_table.='<td class="'.$special_name.'">'.
                                            $character['stats']["special"][$special_name].'</td>
                        ';
                        }
            $special_table.='</tr>
                    </table>';
        return $special_table;
    }

    function generate_skill_table_checkboxed($character){
        $skill_table='
        <table class="skills">
                <tr>
                    <td colspan="2"> Skills:</td>
                    <td>Tag</td>
                </tr>
                ';
        foreach(array_keys($character['stats']["skills"])as $skill){
            $skill_table.='<tr>
                            <td>'.$skill.'</td>
                            <td>'.$character['stats']["skills"][$skill].'</td>
                            <td> <input name="'.$skill.'tag" type="checkbox"></td>
                           </tr>';
        }
        $skill_table.='</table>';
        return $skill_table;
    }

    //Level Up
    function generate_skill_table_skillpoints($character){
        $skill_table='<table class="skills">
                <tr>
                    <td> Skills:</td>
                    <td>Points Left: <span name="left" class="leftovervalue">'.$character['stats']['skill_points'].'</span></td>
                </tr>
                ';
        foreach(array_keys($character['stats']["skills"])as $skill){
            $skill_table.='<tr>
                            <td><input type="button" value="+" onclick="stat_incr(\''.$skill.'\',\''.$character['uid'].'\',true,true)"/>'
                            .$skill
                            .'<input type="button" value="-" onclick="stat_decr(\''.$skill.'\',\''.$character['uid'].'\','.$character['stats']['skills'][$skill].')"/></td>
                            <td name="'.$skill.'" class="'.$skill.'">'.$character['stats']["skills"][$skill].'</td>
                           </tr>';
        }
        $skill_table.='</table>';
        return $skill_table;
    }

    //character_sheet
    function generate_presonal_info_table($character){
        $personal_information_table='
            <table class="personal_info">
            <tr>
            </tr>
            <tr>
            <td>HP: '.$character['stats']["hp"][0].'/'.$character['stats']['hp'][1].'</td>
            <td>SQ: '.$character['stats']["sq"].'</td>
            <td>AP: '.$character['stats']["ap"].'</td>
            <td>AC: '.$character['stats']["ac"].'</td>
            <td>MD: '.$character['stats']["md"].'</td>
            <td>Crit.: '.$character['stats']["cc"].'</td>
            </tr>
            <tr>
            <td>DT/DR: '.$character['stats']["dt"].'/'.$character['stats']["dr"].'</td>
            <td>Recistance</td>
            <td>Rad: '.$character['stats']['rad_res'].'</td>
            <td>Poison: '.$character['stats']['pois_res'].'</td>
            <td>Gas: '.$character['stats']['gas_res'][0].'/'.$character['stats']['gas_res'][1].'</td>
            <td>Electric: '.$character['stats']['elec_res'].'</td>
            </tr>
            <tr>
            <td>Level: '.$character['stats']["level"].'</td>
            <td>XP: '.$character['stats']["xp"].'</td>
            <td>Karma: '.$character['stats']["karma"].'</td>
            <td>Caps: '.$character['stats']['caps'].'</td>
            <td>'.$character['stats']["sex"].'</td>
            <td>'.$character['stats']["race"].'</td>
            </tr>
            </table>';
        return $personal_information_table;
    }

    function generate_effects_table($character){
        $effects_table='<table class="effects_table"><tr><td colspan="2">Effects</td></tr>
            <tr><td>In</td><td>out</td></tr>';
        $rows=(count($character['effects']['in'])+count($character['effects']['out']));
        for ($i=0;$i<$rows;$i++){ //counter for looping over out effect at the same time as in effect.
            $effects_table.='<tr>';
            if (isset($character['effects']['in'][$i])){
                $effect=$character['effects']['in'][$i];
                $effects_table.='<td><input type="button" value="'.$effect.'" onclick="set_effect_out(\''.$character['uid'].'\',this.value)"></td>';
            }else{
                $effects_table.='<td class="EmptyTableCell"></td>';
            }
            if (isset ($character['effects']['out'][$i])){
                $effects_table.='<td><input type="button" value="'.$character['effects']['out'][$i].'" onclick="set_effect_out(\''.$character['uid'].'\',this.value)"></td>';
            }else{
                $effects_table.='<td class="EmptyTableCell"></td>';
            }
            $effects_table.='</tr>';
        } 
        $effects_table.='</table>';
        return $effects_table;
    }

    function generate_equip_table($character){
        $equip_table='<table class="equipped">
            <tr>
            <td colspan="2"class="EmptyTableCell"></td>
            <td colspan="1"><br><input type="button" class="head" value="'.$character["equipped"]["head"].'"onclick="item_action(this,\''.$character["uid"].'\')"><br><br></td>
            </tr>
            <tr>
            <td class="EmptyTableCell"><br></td>
            <td><input type="button" class="rightarm" value="'.$character["equipped"]["rightarm"].'"onclick="item_action(this,\''.$character["uid"].'\')"></td>
            <td rowspan="3"><input type="button" class="body" value="'.$character["equipped"]["body"].'"onclick="item_action(this,\''.$character["uid"].'\')"></td>
            <td><input type="button" class="leftarm" value="'.$character["equipped"]["leftarm"].'"onclick="item_action(this,\''.$character["uid"].'\')"></td>
            </tr>
            <tr>
            <td><input type="button" class="righthand" value="'.$character["equipped"]["righthand"].'" onclick="item_action(this,\''.$character["uid"].'\')"></td>
            <td></td>
            <td></td>
            <td><input type="button" class="lefthand" value="'.$character["equipped"]["lefthand"].'"onclick="item_action(this,\''.$character["uid"].'\')"></td>
            </tr>
            <tr>
            <td class="EmptyTableCell"></td>
            <td><br><br><br><br><br><input type="button" class="leftleg" value="'.$character["equipped"]["leftleg"].'"onclick="item_action(this,\''.$character["uid"].'\')"><br><br></td>
            <td><br><br><br><br><br><input type="button" class="rightleg" value="'.$character["equipped"]["rightleg"].'"onclick="item_action(this,\''.$character["uid"].'\')"><br><br><td>
            </tr>
            </table>';
        return $equip_table;
    }

    function generate_pickup_table($character){
        $weaponlist="";
        $aidlist="";
        $apparellist="";   
        foreach(array_keys($_SESSION['weapons']) as $weapon){
            if($weapon=="none"){//actully should be pickup false, but it's the only one in list
                continue;
            }else{
            $weaponlist.='<option value="'.$weapon.'" >'.$weapon.'</option>
                ';
            }
        }
        foreach(array_keys($_SESSION['aid']) as $aid){
            $aidlist.='<option value="'.$aid.'">'.$aid.'</option>
                ';
        }
        foreach(array_keys($_SESSION['apparel']) as $apparel){
            $apparellist.='<option value="'.$apparel.'">'.$apparel.'</option>
                ';
        }

        $pickup_table='<table class="pickup_table">
            <tr><td><input type="number" value="1" id="PickupAmount'.$character['uid'].'"></td><td><select id="PickupSelect'.$character['uid'].'">
            <optgroup label="---Weapons---">'.$weaponlist
            .'</optgroup><optgroup label="---Aid---">'.$aidlist
            .'</optgroup><optgroup label="---Apparel---">'.$apparellist
            .'</optgroup></select></td>
            <td><input type="button" value="Pick Up" onclick="pickup_item(\''.$character['uid'].'\')"></td></tr></table>';

        return $pickup_table;
    }

    function generate_overlay_div($character,$equip_table){
        $overlay_div='<div id="Overlay-equip'.$character['uid'].'" class="Overlay">
            <table>
            <tr>
            <td class="OverlayTable">'.$equip_table.'</td>
            </tr>
            <tr>
            <td><input type="button" value="cancel" onclick="close_overlay(\''
            .$character['uid']
            .'\',\'equip\')"></td>
            </tr>
            </table>
            </div>

            <div id="Overlay-equipped-item'.$character['uid'].'" class="Overlay">
            <table>
            <tr><td id="item-description'.$character['uid'].'" colspan="3" class="OverlayTable">PlaceHolder for description</td></tr>
            <tr>
            <td class="OverlayTable"><input type="button" value="use" onclick="close_overlay(\''.$character['uid'].'\',\'equipped-item\');fetch_useItem_table(\''.$character['uid'].'\',selectedItem,\'use-item\')"</td>
            <td class="OverlayTable"><input type="button" value="store" onclick="store_item(\''.$character['uid'].'\',selectedItem)"</td>
            <td class="OverlayTable"><input type="button" value="drop" onclick="drop_item(\''.$character['uid'].'\',selectedItem,selectedBodypart)"</td>
            </tr>
            <tr><td colspan="3"><input type="button" value="cancel" onclick="close_overlay(\''.$character['uid'].'\',\'equipped-item\')"></td><tr>
            </table>
            </div>

            <div id="Overlay-use-item'.$character['uid'].'" class="Overlay">
            </div>
            <div id="Overlay-GM-discretion'.$character['uid'].'" class="Overlay">
            <table><tr>
            <td>Stat</td><td>New Value</td>
            </tr><tr>
            <td><select id="GM-discretionStat'.$character['uid'].'">';
        foreach(array_keys($character['stats'])as $stat){
            if(is_array($character['stats'][$stat])){
                $overlay_div.='<optgroup label="'.$stat.'">';
                foreach(array_keys($character['stats'][$stat])as $aStat){
                    $overlay_div.='<option value="'.$stat.'?'.$aStat.'">'.$aStat.'</option>';}
                $overlay_div.='</optgroup>';
            }else{
                $overlay_div.='<option value="'.$stat.'">'.$stat.'</option>';
            }
        }
        $overlay_div.='</select></td><td><input type="text" id="adjustedStatAmount'.$character['uid'].'"></td>
            </tr><tr>
            <td><input type="button" value="Cancel" onclick="close_overlay(\''.$character['uid'].'\',\'GM-discretion\')"></td>
            <td><input type="button" value="Adjust" onclick="adjust_value(\''.$character['uid'].'\')"></td>
            </tr></table></div>
            
            <div id="Overlay-attack-roll'.$character['uid'].'" class="Overlay">
            <table><tr><td>DiceInfoGoes here</td><tr>
            <td><input type="button" value="Cancel" onclick="close_overlay(\''.$character['uid'].'\',\'attack-roll\')"></td>
            </tr></table></div>
            <div id="Overlay-Use-Skill'.$character['uid'].'" class="Overlay">
            <table>';
            foreach (array_keys($_SESSION['skills_list']) as $skill){
                if (isset($_SESSION['skills_list'][$skill]['useable'])){
                    switch ($skill){
                    case "Barter":
                    $overlay_div.='<tr><td><input type="button" value="'.$skill.'" onclick="close_overlay(\''.$character['uid'].'\',\'Use-Skill\');get_skill_overlay(\''.$character['uid'].'\',\''.$skill.'\')"></td><td>'.$_SESSION['skills_list'][$skill]['effect'].'</td></tr>';
                    break;
                    default:
                    $overlay_div.='<tr><td><input type="button" value="'.$skill.'" onclick="close_overlay(\''.$character['uid'].'\',\'Use-Skill\')"></td><td>'.$_SESSION['skills_list'][$skill]['effect'].'</td></tr>';
                    }
                }
            }
        $overlay_div.='</table></div><div id="Overlay-Selected-Skill'.$character['uid'].'" class=Overlay></div>
            <div id="Overlay-die'.$character['uid'].'" class="Overlay">
            <table><tr><td id="targetList'.$character['uid'].'die">Killer: </td><tr>
                <tr><td><input type="button" value="Die" onclick="close_overlay(\''.$character['uid'].'\',\'die\');die(\''.$character['uid'].'\')"><input type="button" value="Cancel" onclick="close_overlay(\''.$character['uid'].'\',\'die\')"></td></tr></table>;
            </div>';
            
        return $overlay_div;
    }

    function generate_gameactions_table($character){
        $gameactions='<table class="gameactions_table">
            <tr>
            <td><input type="button" value="Start Combat" onclick="start_turn(\''.$character['uid'].'\')"></td>
            <td><input type="button" value="Defend" onclick="defend(\''.$character['uid'].'\')"></td>
            <td><input type="button" value="Use Skill" onclick="open_overlay(\''.$character['uid'].'\',selectedItem,\'Use-Skill\')">
            </tr>
            <tr>
            <td><input type="button" value="GM discretion" onclick="open_overlay(\''.$character['uid'].'\',selectedItem,\'GM-discretion\')">
            <td><input type="button" value="Safe" onclick="safe_character(\''.$character['uid'].'\')"></td>
            <td><input type="button" value="Die" onclick="select_target(\''.$character['uid'].'\',\'die\');open_overlay(\''.$character['uid'].'\',\'\',\'die\')"></td>
            </tr>
            </table>';

        return $gameactions;
    }

    function generate_useItem_table($character,$item){
        $useItem_table='
                    <table>';
            if (isset($_SESSION['aid'][$item])){            
                $useItem_table.='<tr><td>'
                        .'<img src="img/vaulttec.png" onload="select_target(\''.$character['uid'].'\');" width=10%" >'
                        .'</td></tr>';
                if (isset ($_SESSION['aid'][$item]['dice'])){
                    $useItem_table.='<tr><td>'
                                    .'roll duration/bonus:<input type="number" id="durationItemRoll'.$character['uid'].'" value=0>'.$_SESSION['aid'][$item]['dice']
                                    .'</td></tr>';
                }
                if (isset ($_SESSION['aid'][$item]['addict'])){
                    $useItem_table.='<tr><td>'
                                    .'roll addiction:<input type="number" id="addictionItemRoll'.$character['uid'].'" value=0>'.$_SESSION['aid'][$item]['addict']
                    .'</td></tr>';
                }
                $useItem_table.='<tr><td id="targetList'.$character['uid'].'">Target:</td></tr>
                        <tr><td><input type="button" value="use" onclick="use_item(\''.$character['uid'].'\')"><input type="button" value="cancel" onclick="close_overlay(\''.$character['uid'].'\',\'use-item\')"></td></tr>';

            }elseif (isset($_SESSION['weapons'][$item])){            
                $useItem_table.=get_attackinfo($character,$item);
            }elseif (isset($_SESSION['apparel'][$_POST['item']])){            
                $useItem_table.='<tr><td>I don\'t understand.. Drop it?</td></tr>
                                <tr><td><input type="button" value="cancel" onclick="close_overlay(\''.$character['uid'].'\',\'use-item\')"></td></tr>';

            }else{
                echo "no description found";
            }
        $useItem_table.='
                       </table> ';
            return $useItem_table;
    }
    
    function generate_use_skill_table($character,$skill){
    $use_skill_table='<table><tr><td>'.$skill.'</td></tr>
                      <tr><td><img src="./img/vaulttec-tiny.png" onload="select_target(\''.$character['uid'].'\',\''.$skill.'\')" width="10%"></td></tr>
                    <tr><td id="targetList'.$character['uid'].$skill.'">Target: </td><tr>
                    <tr><td><input type="button" value="'.$skill.'" onclick="use_skill(\''.$character['uid'].'\',\''.$skill.'\')">
                    <input type="button" value="cancel" onclick="close_overlay(\''.$character['uid'].'\',\'Selected-Skill\')"></td></tr></table>';
    echo $use_skill_table;
    }

    function generate_use_skill_step2($character,$skill,$target){
        $use_skill_table="";
        switch ($skill){
        case "Barter":
        $bonus=$character['stats']["skills"]["Barter"]-$target['stats']["skills"]["Barter"];
        $use_skill_table.='<table><tr><td>'.$character['stats']['name'].' Inventory</td>
                            <td>'.$target['stats']['name'].' Inventory</td></tr><tr><td id="barterCaps'.$character['uid'].'">Caps: '.$character['stats']['caps'].'<td id="barterCaps'.$target['uid'].'">Caps: '.$target['stats']['caps'].'<tr>'
                            .create_barter_inventory($character,$bonus)
                            .create_barter_inventory($target,-$bonus)
                            .'</tr><tr><td><input type="button" value="sell" onclick="add_to_sale(\''.$character['uid'].'\',\''.$target['uid'].'\',\'selling\')">
                            <td><input type="button" value="buy" onclick="add_to_sale(\''.$target['uid'].'\',\''.$character['uid'].'\',\'buying\')"></td></tr>
                            <tr><td id="barterSelling'.$character['uid'].'"></td><td id="barterSelling'.$target['uid'].'"></tr>
                            <tr><td colspan="2"><input type="number" value=0 id="barterCapsExchange"> Caps</td>';
        break;
        default:
        $use_skill_table.='<table><tr><td>"Pussy, No SkillZ"</td></tr>';
        }
        $use_skill_table.='
                        <tr><td colspan="2"><input type="button" value="'.$skill.'" onclick="barterDeal(\''.$character['uid'].'\',\''.$target['uid'].'\')"><input type="button" value="cancel" onclick="close_overlay(\''.$character['uid'].'\',\'Selected-Skill\')"></td></tr></table>';
        echo $use_skill_table;
    }

    function create_barter_inventory($character,$bonus){
       $barter_inv='<td>#: <input id="barterSelectAmount'.$character['uid'].'" type="number" value=1><select id="barterSelect'.$character['uid'].'">';
        foreach (array_keys($character['inv']) as $category){
            if (empty($character['inv'][$category])){
                continue;
            }else{
            $barter_inv.='<optgroup label="'.$category.'">';
            foreach (array_keys($character['inv'][$category]) as $item){
                $value=$_SESSION[$category][$item]["value"];
                $value=round($value+(($value*$bonus)/100));
                $amount=$character['inv'][$category][$item];
                $barter_inv.='<option value="'.$item.'?'.$value.'?'.$amount.'">'.$amount.' - '.$item.' - '.$value.' Caps</option>';
            }
        }}
        $barter_inv.='</select></td>';
        return $barter_inv;
    }

//Processing character action
    //returning character
    function character_equip($character,$item,$bodypart){//equips an item
        //loop inventory for item
        foreach (array_keys($character['inv']) as $category){
            if (isset($character['inv'][$category][$item])){
                //found item in inv/cat do the work
                $character=process_equipment_boost($character,$category,$item);
                //remove item from inventory+ remove completly if item is 0(or less)
                $character['inv'][$category][$item]-=1;
                if ($character['inv'][$category][$item]<=0){
                    unset($character['inv'][$category][$item]);
                }
            }
        }
        //equip item
        if ($character['equipped'][$bodypart]!='none'){
            $eq_item=$character['equipped'][$bodypart];
            $category=find_category($eq_item);
            if (isset($character['inv'][$category][$eq_item])){
                $character['inv'][$category][$eq_item]+=1;
            }else{
                $character['inv'][$category][$eq_item]=1;
            }
        }
        $character['equipped'][$bodypart]=$item;
        //charge AP
        $character['stats']['ap']-=4;
        //update character
        $_SESSION[$character['uid']]=$character;
    }

    function process_equipment_boost($character,$category,$item){
        switch ($category){
            case "weapons":
                //check if minimum requirements are met
                $min_req=explode(":",$_SESSION[$category][$item]['min_req']);
                if ($character['stats']['special'][$min_req[0]]<$min_req[1]){
                    character_say('Unable to use '.$item.' minimum requirment not met '.$min_req[0].$min_req[1]);
                    return $character;
                }
                break;
            case "apparel":
                //apply armor stats
                $character['stats']['ac']+=$_SESSION[$category][$item]['ac']; 
                $character['stats']['dt']+=$_SESSION[$category][$item]['dt']; 
                $character['stats']['dr']+=$_SESSION[$category][$item]['dr']; 
                //apply skill boosts
                if (isset($_SESSION[$category][$item]['skill'])){
                    $skill_perks=explode(",",$_SESSION[$category][$item]['skill']);
                    foreach ($skill_perks as $skill_boost){
                        $skill=explode(":",$skill_boost);
                        $character['stats']['skills'][$skill[0]]+=$skill[1];
                    }
                }
                if (isset($_SESSION[$category][$item]['bonus'])){
                    $bonus_perks=explode(",",$_SESSION[$category][$item]['bonus']);
                    foreach ($bonus_perks as $bonus_boost){
                        $bonus=explode(":",$bonus_boost);
                        $character['stats'][$bonus[0]]+=$bonus[1];
                    }
                }
                break;
                //case "aid": aid items only take effect when used.
        }
        return $character;
    }
    
    function character_drop($character,$item,$bodypart){
        //check dropped from where? inventory=-2 AP
        if ($bodypart=="inventory"){
            //loop inventory for item
            foreach (array_keys($character['inv']) as $category){
                if (isset($character['inv'][$category][$item])){
                    //found item in inv/cat do the work
                    $character['inv'][$category][$item]-=1;
                    if ($character['inv'][$category][$item]<=0){
                        unset($character['inv'][$category][$item]);
                    }
                    $character['stats']['inv_weight']-=$_SESSION[$category][$item]['weight'];
                    $character['stats']['ap']-=2;
                }
            }
        }else{
            $character['equipped'][$bodypart]="none";
            //disable boosts and perks for appearel
            //check if dropped item is apparel
            if (isset($_SESSION['apparel'][$item])){
                $character['stats']['inv_weight']-=$_SESSION['apparel'][$item]['weight'];
                $character['stats']['ac']-=$_SESSION['apparel'][$item]['ac']; 
                $character['stats']['dt']-=$_SESSION['apparel'][$item]['dt']; 
                $character['stats']['dr']-=$_SESSION['apparel'][$item]['dr']; 
                //apply skill boosts
                if (isset($_SESSION['apparel'][$item]['skill'])){
                    $skill_perks=explode(",",$_SESSION['apparel'][$item]['skill']);
                    foreach ($skill_perks as $skill_boost){
                        $skill=explode(":",$skill_boost);
                        $character['stats']['skills'][$skill[0]]-=$skill[1];
                    }
                }
                if (isset($_SESSION['apparel'][$item]['bonus'])){
                    $bonus_perks=explode(",",$_SESSION['apparel'][$item]['bonus']);
                    foreach ($bonus_perks as $bonus_boost){
                        $bonus=explode(":",$bonus_boost);
                        $character['stats'][$bonus[0]]-=$bonus[1];
                    }
                }
            }
            if (isset($_SESSION['weapons'][$item])){
                $character['stats']['inv_weight']-=$_SESSION['weapons'][$item]['weight'];
            }
            if (isset($_SESSION['aid'][$item])){
                $character['stats']['inv_weight']-=$_SESSION['aid'][$item]['weight'];
            }

        }
        $_SESSION[$character['uid']]=$character;
        return $character;
    }

    function use_effects($character,$effects,$roll_duration=0,$roll_addication=0,$clean=FALSE){
        $effects_array=explode(",",$effects);
        foreach($effects_array as $effect_string){
            $effect=explode(":",$effect_string);
            if ($clean===TRUE){$effect[1]=(-1*$effect[1]);}
            if ($effect[0]=="hp"){//for stimpak-use duration as bonus
                $character['stats']['hp'][0]+=($effect[1]+$roll_duration);
                if ($character['stats']['hp'][0]>$character['stats']['hp'][1]){
                    $character['stats']['hp'][0]=$character['stats']['hp'][1];
                }
            }elseif(isset($character['stats'][$effect[0]])){
                $character['stats'][$effect[0]]+=($effect[1]+$roll_duration);
            }elseif(isset($character['stats']['special'][$effect[0]])){//for chems
                if ($character['beast']===TRUE){ return $character;}//beasts are not s.p.e.c.i.a.l
                $character['stats']['special'][$effect[0]]+=$effect[1];
                $character=calculate_skills($character);
                $character=calculate_secondary_stats($character);
            }elseif(isset($character['stats']['skills'][$effect[0]])){//for skills
                if (isset ($character['beast'])){ return $character;}//beasts do not have skills 
                $character['stats']['skills'][$effect[0]]+=$effect[1];
            }
        }
        return $character;
    }

    //rewriting Session['uid']
    function character_use($character,$item,$roll_duration=0,$roll_addication=0,$inout,$bodypart,$target){//apply effects of aid_item
        if ($character['uid']==$target['uid']){
            $target=& $character;
        }
        if (isset($_SESSION['aid'][$item])){
            if ($inout=="in"){
                if (in_array($item,$target['effects']['in'])){//chem already active
                    return $target;
                }
                if (isset($_SESSION['aid'][$item]['out'])){
                    array_push($target['effects']['in'],$item);
                    $item_index=array_search($item,$target['effects']['out']);
                    if ($item_index!==FALSE){//rebouncing from out to in
                        $target=use_effects($target,$_SESSION['aid'][$item]['out'],$roll_duration,$roll_addication,TRUE);
                        unset ($target['effects']['out'][$item_index]);
                    }
                }
                $target=use_effects($target,$_SESSION['aid'][$item]['in'],$roll_duration,$roll_addication);
            $character['stats']['ap']-=3;
            $character=character_drop($character,$item,$bodypart);
            }elseif($inout=="out"){
                $item_index=array_search($item,$target['effects']['out']);
                if ($item_index!==FALSE){//going from Effect out to completly clean
                        $target=use_effects($target,$_SESSION['aid'][$item]['out'],$roll_duration,$roll_addication,TRUE);
                        unset ($target['effects']['out'][$item_index]);
                }else{//going from effect in to out
                unset ($target['effects']['in'][$item_index]);
                $target=use_effects($target,$_SESSION['aid'][$item]['in'],$roll_duration,$roll_addication,TRUE);
                $target=use_effects($target,$_SESSION['aid'][$item]['out'],$roll_duration,$roll_addication);
                array_push($target['effects']['out'],$item);
                }
            }
        }elseif(isset($_SESSION['apparel'][$item])){
            echo "What? just Drop it then?....";
        }elseif(isset($_SESSION['weapons'][$item])){
            echo "What? just Drop it then?....";
        }
        $_SESSION[$character['uid']]=$character;
        $_SESSION[$target['uid']]=$target;
    }

    function character_pickup($character,$item,$amount,$onbody=FALSE){
        $category=find_category($item);
        $character['stats']['inv_weight']+=($_SESSION[$category][$item]['weight']*$amount);
        if (!isset($character['inv'][$category][$item])){
            $character['inv'][$category][$item]=0;
        }
        $character['inv'][$category][$item]+=$amount;
        if ($onbody===FALSE){//being used for storeItem
            foreach(array_keys($character['equipped']) as $bodypart){
                if (($bodypart=="righthand"||$bodypart=="lefthand")&&$character['equipped'][$bodypart]=="none"){
                        $amount-=1;
                        character_equip($character,$item,$bodypart);
                        return;
                }
            }   
        }
        $character['stats']['ap']-=4;
        $_SESSION[$character['uid']]=$character;
    }
    
    function character_refresh($character){
        $character=calculate_secondary_stats($character); 
        $character=calculate_skills($character);
       //calculate boosts from equiped items and apply
        foreach($character['equipped'] as $item){
            if ($item=="none"){
                continue;
            }else{
                foreach (array_keys($character['inv']) as $category){//weapons,apparel/aid
                    if (isset($_SESSION[$category][$item])){
                        //found item in cat do the work
                        $character=process_equipment_boost($character,$category,$item);
                    }       
                }
            }
        }
        $_SESSION[$character['uid']]=$character;
    }

    function find_category($item){
        $category="weapons";
        if (isset($_SESSION['aid'][$item])){
            $category="aid";
        }else
        if (isset($_SESSION['apparel'][$item])){
            $category="apparel";
        }
        return $category;
    }

    function give_good($buyer,$seller,$goods){
        $tmplist=explode(",",$goods);
        foreach ($tmplist as $item){
                $characteritemlist[]=explode("-",$item);
        }
        foreach ($characteritemlist as $item){
           character_pickup($buyer,trim($item[1]),$item[0],TRUE);
            for ($i=0;$i<$item[0];$i++){
               $seller=character_drop($seller,trim($item[1]),"inventory");
            }
        }    
    }

    function barterDeal($character,$characterList,$target,$targetList,$caps)
    {
        if (!empty ($characterList)){
            give_good($character,$target,$characterList);
            //reload character and target after goods have been exchanged cause by refrence is fucked in PHP
            $character=$_SESSION[$character['uid']];
            $target=$_SESSION[$target['uid']];
        }
        if (!empty ($targetList)){
            give_good($target,$character,$targetList);
            //reload character and target after goods have been exchanged cause by refrence is fucked in PHP
            $character=$_SESSION[$character['uid']];
            $target=$_SESSION[$target['uid']];
        }
        $character['stats']['caps']-=$caps;
        $target['stats']['caps']+=$caps;
        $_SESSION[$character['uid']]=$character;   
        $_SESSION[$target['uid']]=$target;   
        character_say("Nice doing Business");
    }
//Character sheet build creation

    function build_character_creation_sheet($character=FALSE){ //builds character creation screen
        //generate fresh character init_character()
            if (!$character){
                $character=init_character();
            }
        //generate personal information table <table class="personal_info">
            $personal_information_table= generate_presonal_info_table_creation($character);
        //generate skills_table
            $skill_table=generate_skill_table_checkboxed($character);
        //generate special table
            $special_table=generate_special_table_creation($character);
        //build charactersheet 
            $charactersheet='<div class="CharacterSheet" id="'.$character['uid'].'">
                    <input type="hidden" name="character_uid" value="'.$character['uid'].'">'
                    .$personal_information_table
                    .$special_table
                    .$skill_table.'<input type="button" value="Create" onclick="character_create(\''.$character['uid'].'\')">
            </div>';
            //page
            return $charactersheet;
    }

    function build_charactersheet($character=NULL){//builds charactersheets
        //build charactersheet, if no character is passed, setup character creation - shares a lot of code with build_character_creation_sheet, maybe merge popping an array to append buttons on the place.
            if ($character==NULL){
            return build_character_creation_sheet();
            }else{
        //generate personal information table <table class="personal_info">
            $personal_information_table= generate_presonal_info_table($character);
        //generate effects_table
            $effects_table=generate_effects_table($character);
        //generate skills_table
                $skill_table='
                <table class="skills">
                        <tr>
                            <td colspan="2"> Skills:</td>
                        </tr>
                        ';
                foreach(array_keys($character['stats']["skills"])as $skill){
                    $skill_table.='<tr>
                                    <td>'.$skill.'</td>
                                    <td> '.$character['stats']["skills"][$skill].' </td>
                                   </tr>';
                }
                $skill_table.='</table>';
        //generate special table
            $special_table='
                <table class="special">
                    <tr>
                        <td colspan="7">Special:</td>
                    </tr>
                    <tr>';
                foreach(array_keys($character['stats']["special"]) as $special_name){
                        $special_table.='
                            <td>'.$_SESSION['special_full'][$special_name].'</td>';
                        }
            $special_table.='</tr>';
                        foreach(array_keys($character['stats']["special"]) as $special_name){
                          $special_table.='<td>'.$character['stats']["special"][$special_name].'</td>
                        ';
                        }
            $special_table.='</tr>
                    </table>';
        //generate inventory table --shit fest, cleanup when you git gud
            $inventory_table='<table class="inventory_table">
                            <tr>
                                <td colspan="9">Inventory <div style="float:right;"> Carrying:'.$character['stats']["inv_weight"].'/'.$character['stats']["carry_weight"].'</div></td>
                            </tr>
                            <tr>';
            //create simplified inventory array (cut out sub associations)
            $cat_keys=["weapons"=>[],"apparel"=>[],"aid"=>[]];
            //loop over primary elements (weapons,apearal,aid)
            foreach(array_keys($character["inv"]) as $category){
                $inventory_table.='<td colspan="3">'.$category.' :</td>';
                //fill simplified inventory array
                foreach(array_keys($character["inv"][$category]) as $item){
                    array_push($cat_keys[$category],$item);
                }
            }
            $inventory_table.='</tr><tr>';
            foreach(array_keys($character["inv"]) as $category){
                $inventory_table.='<td>Name :</td><td>#</td><td>Action</td>';
            }
            $inventory_table.='</tr>';
            //For ALL elements in array (we will endup with one empty row)
            for ($i=0;$i<count($character["inv"],COUNT_RECURSIVE);$i++){
            $inventory_table.='<tr class="InventoryRow">';
                //use simplifies array to define category
                foreach(array_keys($cat_keys) as $category){
                    //check if there is anything left in category
                    if (isset($cat_keys[$category][$i])){
                        $inventory_table.='<td>'.$cat_keys[$category][$i].'</td><td>'
                                            .$character["inv"][$category][$cat_keys[$category][$i]].'</td><td>'.
                                            '<input type="button" value="equip" onclick="open_overlay(\''.$character['uid'].'\',\''.$cat_keys[$category][$i].'\',\'equip\')">'.
                                            '<input type="button" class="inventory" value="drop" onclick="drop_item(\''.$character['uid'].'\',\''.$cat_keys[$category][$i].'\',this.className)">'.
                                            '</td>';
                    }else{ //nothing left in category write empty slot
                    $inventory_table.='<td colspan="3" class="EmptyTableCell"></td>';
                    }
            }
            $inventory_table.='</tr>';
            }
            $inventory_table.='</table>';
        //generate equip_table                
            $equip_table=generate_equip_table($character);                
        //generate pickup_table
            $pickup_table=generate_pickup_table($character);
        //generate game actions table
            $gameactions_table=generate_gameactions_table($character);
        //generate overlay
            $overlay_div=generate_overlay_div($character,$equip_table);
        //build charactersheet 
            $charactersheet='
                    <input type="hidden" name="character_uid" value="'.$character['uid'].'">
                    <h2>'.$character['stats']["name"].'</h2><hr>'
                    .$personal_information_table
                    .$effects_table
                    .$special_table
                    .$skill_table
                    .$inventory_table
                    .$equip_table
                    .$pickup_table
                    .$gameactions_table
                    .$overlay_div
                  ;
            //page
            return $charactersheet;
        }
    }
    
    function build_levelup_sheet($character){
        $charactersheet='<h1>--LEVELUP--</h1> <h2>'.$character['stats']["name"].'</h2><hr>'
        .generate_skill_table_skillpoints($character)
        .'<input type="button" value="LevelUp" onclick="character_lvlup(\''.$character['uid'].'\')">';

        return $charactersheet;
    }
    ?>
