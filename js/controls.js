//JS for buttons
//
//    Author: Lander Verstappen 
//    This file is part of Fpnp-PHP.
//
//    Fpnp-PHP is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Fpnp-PHP is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Fpnp-PHP.  If not, see <http://www.gnu.org/licenses/>.

//Global variables: could be solved by using 2 classes on items
    var selectedItem="";
    var selectedBodypart="";

//Game funtions 
    function new_game(){
        var data=new FormData()
        data.append('data_target',"gamezone");
        data.append('actiontype',"game");
        post_action("new_game",data,put_data);
    }

    function add_player(){
        var data=new FormData()
        data.append('data_target','players');
        data.append('actiontype',"game");
        post_action("add_player",data,append_data);
    }

    function load_player(){
        var id=document.getElementById('add-character-select-list').value;
        var data=new FormData();
        data.append('data_target','players');
        data.append('actiontype',"game");
        data.append('savefile',id);
        post_action("load",data,append_data);
    }

    function add_npc(){
        var select=document.getElementById("add-npc-select-list")
        var option=select.options[select.selectedIndex];
        var npc_class=option.parentNode.label;
        var npc=option.value;
        var data=new FormData();
        data.append ('npc',npc);
        data.append ('npc_class',npc_class);
        data.append('actiontype',"game");
        data.append('data_target','npc_zone');
        post_action('add_npc',data,append_data);
    }

    function safe_character(id){
        var data=new FormData();
        data.append("uid",id);
        data.append("data_target",id);
        data.append("actiontype","game");
        post_action("safe",data,put_data);
    }
    
    function load_session(){
        var data=new FormData()
        data.append('data_target','gamezone');
        data.append('actiontype',"game");
        data.append('name',document.getElementById('load-session').value);
        post_action("load_session",data,put_data);
    }

    function save_session(){
        var data=new FormData()
        data.append('data_target','MenuBar');
        data.append('actiontype',"game");
        data.append('name',document.getElementById('session-name').value);
        post_action("save_session",data,append_data);
    }

//Character Creation/update Functions
    function stat_incr(stat,id,max=10,skill=false) { //increase stat used for character creation needs stat and character id - Race not implemented
        //get player
        var player=document.getElementById(id);
        //get what's left of points to distribute and stat
        var left=parseInt(player.getElementsByClassName('leftovervalue')[0].innerHTML);
        var val=parseInt(player.getElementsByClassName(stat)[0].innerHTML);
        //check for max and min stats else increase stat decrease left - currently set for human only.
        if (left==0 || val==max){
            alert("nothing left or maximum reached");
        }else if (skill===false){//we're doing character creation not levelup
        player.getElementsByClassName(stat)[0].innerHTML=val+1;
        player.getElementsByClassName('leftovervalue')[0].innerHTML=left-1;
        }else{//levelup, assigning skill points
           var needed_points=1; 
           if (val<101){}
           else if(val<126){needed_points=2;}
           else if(val<151){needed_points=3;} 
           else if(val<176){needed_points=4;}
           else if(val<201){needed_points=5;}
           else{needed_points=6}
          if (needed_points>left){
            alert("insufficient skill points to raise, needs "+needed_points);
          }else{
            player.getElementsByClassName(stat)[0].innerHTML=val+1;
            player.getElementsByClassName('leftovervalue')[0].innerHTML=left-needed_points;
         }
        }
    }

    function stat_decr(stat,id,min=1) {//decrease stat used for character creation needs stat and character id - Race not implemented
        //get player
        var player=document.getElementById(id);
        //get what's left of points to distribute and stat
        var left=parseInt(player.getElementsByClassName('leftovervalue')[0].innerHTML);
        var val=parseInt(player.getElementsByClassName(stat)[0].innerHTML);
        //do the work
        if (val==min){
            alert("minimum reached");
        }else{
        player.getElementsByClassName(stat)[0].innerHTML=val-1;
        player.getElementsByClassName('leftovervalue')[0].innerHTML=left+1;
        }
    }

    function character_create(id){//AJAX to update character needs id uses collect_character_data to create from for post
        //Collect Character data
        var character=collect_character_data(id);
        character.append("data_target",id);
        character.append("actiontype","character");
        post_action("update",character,put_data);
    }

    function collect_character_data(id){ // collects all data from character via id of div(only elements with name that are inside a table)
        //get all tables (character is created from several tables (table.special,table.status,table.skill...)
        var player=Array.prototype.slice.call(document.getElementById(id).getElementsByTagName("table"));
        //create a new form to send to server
        var data=new FormData();
        //start form with chatacter id
        data.append("uid",id);
        //loop through tables>name and put in form
        for (table of player){
            if (table.className=="special"){
                for (stat of Array.prototype.slice.call(table.querySelectorAll("[class]"))){
                    data.append(stat.className,stat.innerHTML);
                }
           }else{
           for (stat of Array.prototype.slice.call(table.querySelectorAll("[name]"))){
                if (stat.type=="checkbox"){
                  data.append(stat.name,stat.checked);
                }else{
                  data.append(stat.name,stat.value);   
                }
            }
            }      
        }   
        return data;
    }
    
    function character_lvlup(id){
        var data=new FormData;
        data.append("uid",id);
        data.append("data_target",id);
        data.append("actiontype","character");
        var player=Array.prototype.slice.call(document.getElementById(id).getElementsByTagName("table"));
        for (table of player){
           for (stat of Array.prototype.slice.call(table.querySelectorAll("[name]"))){
                    data.append(stat.className,stat.innerHTML);
            }
        }
        post_action("levelup",data,put_data);
    
    }
//Character-specific 
    //Items
    function pickup_item(id){
        var item=document.getElementById("PickupSelect"+id);
        var amount=document.getElementById("PickupAmount"+id);
        var data=new FormData();
        data.append("uid",id);
        data.append("item",item.value);
        data.append("amount",amount.value);
        data.append("data_target",id);
        data.append("actiontype","character");
        post_action("pickup",data,put_data);
     }

    function item_equip(id,bodypart){
        close_overlay(id,'equip');
        var data=new FormData();
        data.append("uid",id);
        data.append("item",selectedItem);
        data.append("bodypart",bodypart);
        data.append("data_target",id);
        data.append("actiontype","character");
        post_action("equip",data,put_data);
    }

    function item_action(item,id){
        //data.append('item',item);
        var parentclass=item.parentNode.parentNode.parentNode.parentNode.parentNode.className;
        if (parentclass=="OverlayTable"){
            item_equip(id,item.className);
        }else if(parentclass=="CharacterSheet"){
            var data=new FormData();
            data.append("item",item.value);
            data.append("data_target","item-description"+id);
            data.append("actiontype","character");
            post_action("item_info",data,put_data); 
            open_overlay(id,item.value,'equipped-item');
            selectedBodypart=item.className;
        }
    }

    function fetch_useItem_table(id,item,overlay){
        var data=new FormData();
        data.append("uid",id);
        data.append("item",item);
        data.append("data_target","Overlay-"+overlay+id);
        data.append("actiontype","character");
        post_action("item_table",data,put_data);
        open_overlay(id,item,overlay);
    }

    function drop_item(id,item,bodypart){
        console.log(id,item,bodypart);
        var data=new FormData();
        data.append("uid",id);
        data.append("item",item);
        data.append("bodypart",bodypart);
        data.append("data_target",id);
        data.append("actiontype","character");
        post_action("drop",data,put_data);
    }

    function use_item(id){
            var data=new FormData();
            if (document.getElementById("durationItemRoll"+id)!==null){
                var duration=document.getElementById("durationItemRoll"+id).value;
                data.append("duration",duration);
            }
            if (document.getElementById("addictionItemRoll"+id)!==null){
                var addiction=document.getElementById("addictionItemRoll"+id).value;
                data.append("addiction",addiction);
            }
            var target=document.getElementById("SelectedTarget"+id).value;
            data.append("uid",id);
            data.append("item",selectedItem);
            data.append("bodypart",selectedBodypart);
            data.append("inout","in");
            data.append("target",target);
            data.append("data_target",id);
            data.append("actiontype","character");
            if(id!=target){
                console.log("targeted "+target);
                post_action("use",data,put_data,true);
                //request_refresh(target,put_data);
            }else{
            post_action("use",data,put_data);
            }
    }

    function store_item(id,item){
                var data=new FormData();
                data.append("uid",id);
                data.append("item",selectedItem);
                data.append("bodypart",selectedBodypart);
                data.append("data_target",id);
                data.append("actiontype","character");
                post_action("store",data,put_data);
    }

    function set_effect_out(id,item){
            var data=new FormData();
            data.append("uid",id);
            data.append("target",id);
            data.append("item",item);
            data.append("data_target",id);
            data.append("inout","out");
            data.append("actiontype","character");
            post_action("use",data,put_data);
    }

    //Combat
    function character_getChanceToHit(id){
        console.log('changing RTH');
        var data=new FormData();
        data.append("uid",id);
        data.append("range",document.getElementById("attackRange"+id).value);
        data.append("skill",document.getElementById("attackSkill"+id).value);
        data.append("weapon",selectedItem);
        data.append("mode",document.getElementById("attackMode"+id).value);
        data.append("target",document.getElementById("SelectedTarget"+id).value);
        data.append("data_target","chanceToHit"+id);
        data.append("actiontype","combat");
        data.append("targeted",document.getElementById("attackTargeted"+id).value);
        post_action("chanceToHit",data,put_data);
    }
    
    //skills    
    function get_skill_overlay(id,skill){
        var data=new FormData();
        console.log(id+skill);
        data.append("uid",id);
        data.append("skill",skill);
        data.append("data_target","Overlay-Selected-Skill"+id);
        data.append("actiontype","character");
        post_action("use-skill",data,put_data);
        open_overlay(id,selectedItem,'Selected-Skill');
    }

    function use_skill(id,skill){
        var target=document.getElementById("SelectedTarget"+id+skill).value;
        var data=new FormData();
        data.append("uid",id);
        data.append("skill",skill);
        data.append("target",target);
        data.append("data_target","Overlay-Selected-Skill"+id);
        data.append("actiontype","character");
        post_action("use-skill-step2",data,put_data);
    }

//character-npc shared functions --Mostly combat
    function get_attackinfo(id,weapon){
        var target=document.getElementById("SelectedTarget"+id).value;
        var data=new FormData();
        data.append("uid",id);
        data.append("weapon",weapon);
        data.append("target",target);
        data.append("actiontype","combat");
        data.append("data_target","Overlay-attack-roll"+id);
        post_action("get_attackinfo",data,append_data);
    }

    function attack(id,weapon){
        var attackRoll=document.getElementById("attackRoll"+id);
        var target=document.getElementById("SelectedTarget"+id).value;
        if (attackRoll.getAttribute("class")!=null){
        console.log("NPC attack");
        var cost=attackRoll.getAttribute("class");
        }else{
        var index=document.getElementById("attackMode"+id).selectedIndex;
        var mode=document.getElementById("attackMode"+id)[index].innerHTML;
        var ap=mode.substring(mode.length-4);
        var cost=ap.substring(0,2);
        }
        
        var data=new FormData();
        data.append("uid",id);
        data.append("weapon",weapon);
        data.append("cost",cost);
        data.append("attackRoll",attackRoll.value);
        data.append("target",target);
        data.append("data_target",id);
        data.append("actiontype","combat");
        post_action("attack",data,put_data,true);
    }

    function start_turn(id){
        var data=new FormData();
        data.append("uid",id);
        data.append("data_target",id);
        data.append("actiontype","combat");
        post_action("start_turn",data,put_data);
    }

    function defend(id){
        var data=new FormData();
        data.append("uid",id);
        data.append("data_target",id);
        data.append("actiontype","combat");
        post_action("defend",data,put_data);
    }
    
    function die(id){
        var killme=document.getElementById(id);
        var killmeparent=killme.parentNode;
        var killer=document.getElementById('SelectedTarget'+id+'die').value;
        if (id!=killer){//don't bug server on suicide - character will stay in session
        var data=new FormData();
        data.append("uid",id);
        data.append("data_target",killer);
        data.append("actiontype","combat");
        post_action("die",data,put_data);
        }
        killmeparent.removeChild(killme);
    }
    
    function adjust_value(id){
            close_overlay(id,'GM-discretion');
            var data=new FormData();
            data.append("uid",id);
            data.append("stat",document.getElementById("GM-discretionStat"+id).value);
            data.append("amount",document.getElementById("adjustedStatAmount"+id).value);
            data.append("data_target",id);
            data.append("actiontype","game");
            post_action("adjust_stat",data,put_data);
    }

    function select_target(id,skill=""){
        console.log("Selecting "+id+skill);
        if (document.getElementById("SelectedTarget"+id+skill)){
            var targetSelect=document.getElementById("SelectedTarget"+id+skill);
            document.getElementById("targetList"+id+skill).removeChild(targetSelect);
            console.log("removed previous list");
        }
        var targetSelect=document.createElement("select");
        targetSelect.id="SelectedTarget"+id+skill;
        var availableTargets=Array.prototype.slice.call(document.getElementsByClassName("CharacterSheet"));
        availableTargets.forEach(function (availableTarget){
                var name=document.getElementById(availableTarget.id).getElementsByTagName("h2")[0].innerHTML;
                if(availableTarget.id==id){targetSelect.insertAdjacentHTML("beforeend",'<option value="'+id+'" selected>On Self</option>');
                }else{
                targetSelect.insertAdjacentHTML("beforeend","<option value="+availableTarget.id+">"+name+"</option>");}
                });
        document.getElementById("targetList"+id+skill).appendChild(targetSelect);
    }

//Helper functions -Layout controll
    function open_overlay(id,item,select){
        var overlay= document.getElementById('Overlay-'+select+id);
        overlay.style.visibility="visible";
        selectedItem=item;
    }
        
    function close_overlay(id,select){
        var overlay= document.getElementById('Overlay-'+select+id);
        overlay.style.visibility="hidden";
    }

    function show_npc_description(id){
        var description= document.getElementById(id+'description');
        if (description.style.display=="none"){
            description.style.display="table-cell";
        }else{description.style.display="none";}
    }

    function clean_attack_fetched_info(id){
        var container=document.getElementById('Overlay-attack-roll'+id);
        var info=document.getElementById('fetched_info'+id);
        container.removeChild(info);
    }

    function put_data(data_target,data){//rewrite data_target with data
        document.getElementById(data_target).innerHTML=data;
    }

    function append_data(data_target,data){//append data_target with data
        document.getElementById(data_target).insertAdjacentHTML('beforeend',data);
    }

    function post_action(action,data,callback,refresh=false){//uses FormData() check crowser compatibility 
            data.append("action",action);
        //Create request 
            var post_action = new XMLHttpRequest();
            post_action.open("POST", "./php/action.php", true);
        //register
            post_action.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                   callback(data.get("data_target"),post_action.response);
                    if (refresh==true){
                        request_refresh(data.get("target"),put_data);}
                }
            };
        //work it
            post_action.send(data);
    }

    function request_refresh(id,callback){//When 2 AJAX-calls/responses are needed.
            var refresh_data=new FormData();
            refresh_data.append("action","refresh");
            refresh_data.append("actiontype","game");
            refresh_data.append("uid",id);
        //Create request 
            var request_refresh = new XMLHttpRequest();
            request_refresh.open("POST", "./php/action.php", true);
        //register
            request_refresh.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                   callback(refresh_data.get("uid"),request_refresh.response);
                }
            };
        //work it
            request_refresh.send(refresh_data);
    }
    function set_attackTargeted_visible(id){
        document.getElementById("attackTargetedRow"+id).style.display="inline";
    }

    function add_to_sale(buyer,seller,buysell){
        //get all info from table
        var good=document.getElementById("barterSelect"+buyer)
        var item=good.value.split("?")[0];
        var price=parseInt(good.value.split("?")[1]);
        var instock=parseInt(good.value.split("?")[2]);
        var amount=parseInt(document.getElementById("barterSelectAmount"+buyer).value);
        var exchange=document.getElementById("barterCapsExchange");
        //calculate cariables
        var left=instock-amount;
        var select=good.selectedIndex;
        //set caps topay, to get
        if (buysell=="buying"){
            exchange.value=parseInt(exchange.value)+(price*amount);
        }else{
            exchange.value=parseInt(exchange.value)-(price*amount);
        }
        //Update select
        good[select].value=item+"?"+price+"?"+left;
        good[select].innerHTML=left+" - "+item+" - "+price;
        //remove item from list when it is zero or lower
        if (left<=0){
        good[select].parentNode.removeChild(good[select]);
        }
        

        console.log("Item: "+item+" Price: "+price+" Amount: "+amount);
        //Create List item of product
        var data='<li class="barterSelling'+seller+item+'">'+amount+' - '+item+' - '+amount*price+' Caps <input type="button" value="Remove" onclick="cancelItemSale(\''+amount+'\',\''+item+'\',\''+price+'\',\''+seller+'\',\''+buyer+'\',\''+buysell+'\')"/></li>';
        //Put item in selling cell
        append_data("barterSelling"+seller,data);
    }

    function cancelItemSale(amount,item,price,seller,buyer,buysell){
        var good=document.getElementsByClassName("barterSelling"+seller+item)[0];
        var exchange=document.getElementById("barterCapsExchange");
        var storelist=document.getElementById("barterSelect"+buyer);
        console.log(good);
        document.getElementById("barterSelling"+seller).removeChild(good);
        var option=document.createElement("option");
        option.value=item+"?"+price+"?"+amount;
        option.innerHTML=amount+" - "+item+" - "+price+" Caps";
        storelist.appendChild(option);
        if (buysell=="buying"){
            exchange.value=parseInt(exchange.value)-(price*amount);
        }else{
            exchange.value=parseInt(exchange.value)+(price*amount);
        }
    }

    function barterDeal(id,target){
        //Gather Data
        var storeListPlayer=new Array;
        var storeListTarget=new Array;
        var caps=document.getElementById("barterCapsExchange").value;
        var storeListPlayerNode=Array.prototype.slice.call(document.getElementById("barterSelling"+id).getElementsByTagName("li"));
        var storeListTargetNode=Array.prototype.slice.call(document.getElementById("barterSelling"+target).getElementsByTagName("li"));
        for(item in storeListPlayerNode){storeListPlayer.push(storeListPlayerNode[item].innerText);}
        for(item in storeListTargetNode){storeListTarget.push(storeListTargetNode[item].innerText);}
        //send data to Server
        var data=new FormData;
        data.append("uid",id);
        data.append("target",target);
        data.append("PlayerList",storeListPlayer);
        data.append("TargetList",storeListTarget);
        data.append("caps",caps);
        data.append("actiontype","character");
        data.append("data_target",id);
        //ideas are dangerous, okay
        document.getElementById("barterCapsExchange").parentNode.removeChild(document.getElementById("barterCapsExchange"));
        post_action("barterDeal",data,put_data,true);
        close_overlay(id,'Selected-Skill');
    }
